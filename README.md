# Bitbucket Pipelines Pipe: Email Notify

Send email with a specific message.

## YAML Definition

Add the following snippet to the after-scripts section of your `bitbucket-pipelines.yml` file:           
    
```yaml
- pipe: atlassian/email-notify:0.2.0
  variables:
    USERNAME: '<string>'
    PASSWORD: '<string>'
    FROM: '<string>'
    TO: '<string>'
    HOST: '<string>'
    # PORT: '<string>' # Optional.
    # TLS: '<boolean>' # Optional.
    # SUBJECT: '<string>' # Optional.
    # BODY_PLAIN: '<string>' # Optional.
    # BODY_HTML: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable                   | Usage                                                |
| ----------------------------- | ---------------------------------------------------- |
| USERNAME (*)              | The username to authenticate with. |
| PASSWORD (*)          | The password to authenticate with. |
| FROM (*)           | The email address to send email from. |
| TO (*)           | The email address to send email to. |
| HOST (*)            | The remote host with SMTP server to connect to. |
| PORT            | The port of the remote host with SMTP server to connect to. Default: `587`|
| TLS            | Put the SMTP connection in TLS (Transport Layer Security) mode. Default: `true`|
| SUBJECT            | The subject of the email. Default: `Bitbucket Pipe Notification for ${BITBUCKET_BRANCH}`.|
| BODY_PLAIN            | Text that stored in the body part of the email as 'plain' text. Default: `Email send from Bitbucket Pipeline <a href='https://bitbucket.org/${BITBUCKET_WORKSPACE}/${BITBUCKET_REPO_SLUG}/addon/pipelines/home#!/results/${BITBUCKET_BUILD_NUMBER}'>build#${BITBUCKET_BUILD_NUMBER}</a>`|
| BODY_HTML            | The name of file with html content that will be in the body part of the email as 'html'. This requires a template file to be present in your repository. |
| DEBUG                 | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Details

Email-notify connects to SMTP server provided in HOST variable.

## Prerequisites

To use Email-notify you need to choose your favorite SMTP server.
 
## Examples

### Basic example:
    
```yaml
scripts:
  - pipe: atlassian/email-notify:0.2.0
    variables:
      USERNAME: 'myemail@example.com'
      PASSWORD: $PASSWORD
      FROM: 'myemail@example.com'
      TO: 'example@example.com'
      HOST: 'smtp.gmail.com'
      PORT: 587
```

### Advanced examples:
Here we pass extra arguments to the email-notify command to use custom email's subject and enable extra debugging.
    
```yaml
scripts:
  - pipe: atlassian/email-notify:0.2.0
    variables:
      USERNAME: 'myemail@example.com'
      PASSWORD: $PASSWORD
      FROM: 'myemail@example.com'
      TO: 'example@example.com'
      HOST: 'smtp.gmail.com'
      PORT: 587
      SUBJECT: 'Bitbucket Pipe Notification for your-bitbucket-brunch'
      DEBUG: true
```

Example with alternate email's body with html template from the file and usage in `after-scripts` part of pipelines.
    
```yaml
after-scripts:
  - pipe: atlassian/email-notify:0.2.0
    variables:
      USERNAME: 'myemail@example.com'
      PASSWORD: $PASSWORD
      FROM: 'myemail@example.com'
      TO: 'example@example.com'
      HOST: 'smtp.gmail.com'
      PORT: 587
      BODY_HTML: 'email_template.html'
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/choose-node/true/interaction-style/qanda?add-tags=bitbucket-pipelines,pipes,email-notify,email,notify
