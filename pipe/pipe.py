import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from bitbucket_pipes_toolkit.helpers import (
    configure_logger, enable_debug,
    required, get_variable, success, fail)


class NotValidVariable(Exception):
    pass


DEFAULT_SMTP_PORT = '587'
DEFAULT_TIMEOUT = '10'
SMTP_OK_STATUS_CODE = 250
STANDARD_SMTP_PORTS = (25, 465, 587, 2525)

DEFAULT_TEXT_REPOSITORY = "Bitbucket Pipe Notification"
DEFAULT_TEXT_MESSAGE = "Email sent from Bitbucket Pipeline"
BASE_SUCCESS_MESSAGE = 'The mail has been sent successfully'
BASE_FAILED_MESSAGE = 'Failed to send email'


def main():
    logger = configure_logger()
    enable_debug()

    # get variables
    username = required('USERNAME')
    password = required('PASSWORD')
    from_email = required('FROM')
    to_email = required('TO')
    host = required('HOST')

    port = get_variable('PORT', default=DEFAULT_SMTP_PORT)
    tls = get_variable('TLS', default='true')

    if port.isdigit() and int(port) not in STANDARD_SMTP_PORTS:
        logger.warning((
            f'Non standard SMTP PORT using: {port}. '
            f'SMTP standard ports are '
            f'{(", ".join(str(i) for i in STANDARD_SMTP_PORTS))}.'
        ))

    debug = get_variable('DEBUG', default="false")
    timeout = get_variable('TIMEOUT', default=DEFAULT_TIMEOUT)

    if timeout is not None and is_valid_timeout(timeout):
        timeout = float(timeout)

    workspace = get_variable('BITBUCKET_WORKSPACE', default='local')
    repo = get_variable('BITBUCKET_REPO_SLUG', default='local')
    build = get_variable('BITBUCKET_BUILD_NUMBER', default='local')
    branch = get_variable('BITBUCKET_BRANCH', default='local')

    default_subject = (
        f'{DEFAULT_TEXT_REPOSITORY}'
        f'{(f" for {branch}" if branch != "local" else f"")}'
    )
    subject = get_variable('SUBJECT', default=default_subject)
    default_body_plain = (
        f"{DEFAULT_TEXT_MESSAGE} "
        f"<a href='https://bitbucket.org/{workspace}/{repo}"
        f"/addon/pipelines/home#!/results/{build}'>build #{build}</a>"
    )

    body_plain = get_variable('BODY_PLAIN', default=default_body_plain)
    body_html_filename = get_variable('BODY_HTML', default=None)
    body_html = body_plain

    if body_html_filename is not None:
        try:
            with open(body_html_filename, 'r') as f:
                body_html = f.read()
        except FileNotFoundError as e:
            fail(message=f'{BASE_FAILED_MESSAGE}: {str(e)}')

    # create a message
    msg = MIMEMultipart('alternative')
    msg['FROM'] = from_email
    msg['TO'] = to_email
    msg['Subject'] = subject

    # send both html and text
    part1 = MIMEText(body_plain, 'plain', _charset='utf-8')
    part2 = MIMEText(body_html, 'html', _charset='utf-8')

    msg.attach(part1)
    msg.attach(part2)

    logger.info('Sending email...')

    result = None

    try:
        smtp = smtplib.SMTP(host, port, timeout=timeout)
        smtp.ehlo()
        if tls == 'true':
            smtp.starttls()
            smtp.ehlo()
        smtp.login(username, password)
        smtp.send_message(msg)
        result = smtp.noop()
        smtp.quit()
    # connection error or timeout
    except OSError as e:
        fail(message=(
            f'{BASE_FAILED_MESSAGE} to {to_email}. '
            f'Check your configuration settings'
            f'{(f": {e}" if debug == "true" else f".")}'
        ))

    if result is None or result[0] != SMTP_OK_STATUS_CODE:
        fail(message=(
            f'{BASE_FAILED_MESSAGE} to {to_email}. '
            f'{(f": response {result}" if debug == "true" else f".")}'
        ))

    success(message=f'{BASE_SUCCESS_MESSAGE} to {to_email}')


def is_valid_timeout(str_value):
    if is_positive_number(str_value):
        return True
    else:
        raise NotValidVariable(
            'Wrong TIMEOUT value. '
            'TIMEOUT must be greater than 0.')


def is_positive_number(str_value):
    try:
        return float(str_value) > 0
    except ValueError:
        return False


if __name__ == '__main__':
    main()
